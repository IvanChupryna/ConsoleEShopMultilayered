﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Order
    {
        public int Id { get; set; }
        public Dictionary<int, int> Items { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Status { get; set; }
        public int UserId { get; set; }

    }
}
