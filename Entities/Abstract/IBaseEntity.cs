﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Abstract
{
    public interface IBaseEntity
    {
        public int Id { get; set; }
    }
}
