﻿using System;
using Entities.Abstract;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class ProductEntity : BaseEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }

        public ProductEntity() { }
        public ProductEntity(int id, string name, decimal price)
        {
            Id = id;
            Name = name;
            Price = price;
        }
    }
}
