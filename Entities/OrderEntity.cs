﻿using Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class OrderEntity : BaseEntity
    {
        public Dictionary<int, int> Items { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }
    }
}
