﻿using System;
using Entities;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories.Abstract
{
    public interface IOrdersRepository
    {
        void AddOrder(OrderEntity order);
        void UpdateOrder(OrderEntity order);
        List<OrderEntity> GetAllOrders();
        List<OrderEntity> GetOrdersForUser(int userId);
        OrderEntity GetOrderById(int orderId);
        void DeleteOrderById(int orderId);
    }
}
