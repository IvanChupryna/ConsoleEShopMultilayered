﻿using System;
using Entities;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories.Abstract
{
    public interface IProductsRepository
    {
        void AddProduct(ProductEntity product);
        void UpdateProduct(ProductEntity product);
        List<ProductEntity> GetAllProducts();
        ProductEntity GetProductByName(string name);
        ProductEntity GetProductById(int productId);
        void DeleteProductById(int productId);
    }
}
