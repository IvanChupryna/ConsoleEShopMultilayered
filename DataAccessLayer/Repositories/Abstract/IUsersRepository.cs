﻿using System;
using Entities;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories.Abstract
{
    public interface IUsersRepository
    {
        public void AddUser(UserEntity user);
        public void UpdateUser(UserEntity user);
        public List<UserEntity> GetAllUsers();
        public UserEntity GetUserByName(string username);
        public UserEntity GetUserByEmail(string userEmail);
        public UserEntity GetUserById(int userId);
        public void DeleteUserById(int userId);
    }
}
