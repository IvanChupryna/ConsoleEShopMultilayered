﻿using DataAccessLayer.Repositories.Abstract;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private static List<UserEntity> _users = new List<UserEntity>()
        {
            new UserEntity("admin", "admin", "admin", true)
        };
        public void AddUser(UserEntity user)
        {
            _users.Add(user);
        }

        public void DeleteUserById(int userId)
        {
            _users.Remove(GetUserById(userId));
        }

        public List<UserEntity> GetAllUsers()
        {
            return _users;
        }

        public UserEntity GetUserById(int userId)
        {
            return _users.FirstOrDefault(ue => ue.Id == userId);
        }

        public UserEntity GetUserByName(string username)
        {
            return _users.FirstOrDefault(ue => ue.Name == username);
        }

        public UserEntity GetUserByEmail(string userEmail)
        {
            return _users.FirstOrDefault(ue => ue.Email == userEmail);
        }
        public void UpdateUser(UserEntity user)
        {
            UserEntity userToUpdate = GetUserById(user.Id);
            userToUpdate.Name = user.Name ?? userToUpdate.Name;
            userToUpdate.Email = user.Email ?? userToUpdate.Email;
            userToUpdate.Password = user.Password ?? userToUpdate.Password;
            userToUpdate.IsAdmin = user.IsAdmin;
        }
    }
}
