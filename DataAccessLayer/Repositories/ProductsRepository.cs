﻿using DataAccessLayer.Repositories.Abstract;
using Entities;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class ProductsRepository : IProductsRepository
    {
        private static List<ProductEntity> _products = new List<ProductEntity>()
        { new ProductEntity(0, "Apple", 3.25m),
          new ProductEntity(1, "Banana", 4),
          new ProductEntity(2, "Grapes", 5)}; 
        public void AddProduct(ProductEntity product)
        {
            _products.Add(product);
        }

        public void DeleteProductById(int productId)
        {
            _products.Remove(GetProductById(productId));
        }

        public List<ProductEntity> GetAllProducts()
        {
            return _products;
        }

        public ProductEntity GetProductById(int productId)
        {
            return _products.FirstOrDefault(pe => pe.Id == productId);
        }

        public ProductEntity GetProductByName(string name)
        {
            return _products.FirstOrDefault(pe => pe.Name == name);
        }

        public void UpdateProduct(ProductEntity product)
        {
            ProductEntity productToUpdate = GetProductById(product.Id);
            productToUpdate.Name = product.Name ?? productToUpdate.Name;
            productToUpdate.Price = product.Price == default(decimal) ? productToUpdate.Price : product.Price;
        }
    }
}
