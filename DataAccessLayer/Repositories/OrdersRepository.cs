﻿using DataAccessLayer.Repositories.Abstract;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class OrdersRepository : IOrdersRepository
    {
        private static List<OrderEntity> _orders = new List<OrderEntity>();
        public void AddOrder(OrderEntity order)
        {
            _orders.Add(order);
        }

        public void DeleteOrderById(int orderId)
        {
            OrderEntity orderToDelete = GetOrderById(orderId);
            _orders.Remove(orderToDelete);
        }

        public List<OrderEntity> GetAllOrders()
        {
            return _orders;
        }

        public OrderEntity GetOrderById(int orderId)
        {
            return _orders.FirstOrDefault(oe => oe.Id == orderId);
        }

        public List<OrderEntity> GetOrdersForUser(int userId)
        {
            return _orders.Where(oe => oe.UserId == userId).ToList();
        }

        public void UpdateOrder(OrderEntity order)
        {
            OrderEntity orderToUpdate = GetOrderById(order.Id);
            orderToUpdate.Items = order.Items;
            orderToUpdate.UserId = order.UserId;
            orderToUpdate.CreatedDate = order.CreatedDate;
            orderToUpdate.Status = order.Status;
        }
    }
}
