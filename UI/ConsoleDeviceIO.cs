﻿using System;
using System.Collections.Generic;
using System.Text;
using UI.Abstract;

namespace UI
{
    class ConsoleDeviceIO : IDeviceIO
    {
        public string Input()
        {
            return Console.ReadLine().Trim();
        }

        public void Output(string output)
        {
            Console.WriteLine(output);
        }

        public void ClearOutput()
        {
            Console.Clear();
        }

        public void Wait()
        {
            Console.ReadKey();
        }
    }
}
