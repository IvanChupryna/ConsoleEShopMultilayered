﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UI.Abstract
{
    public interface IDeviceIO
    {
        string Input();
        void Output(string output);
        void ClearOutput();
        void Wait();
    }
}
