﻿using BusinessLogicLayer.Abstract;
using DataAccessLayer.Repositories.Abstract;
using Domain;
using Entities;
using Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository _usersRepository;

        public UsersService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }
        public void AddUser(User user)
        {
            _usersRepository.AddUser(user.ToEntity());
        }

        public void DeleteUserById(int userId)
        {
            _usersRepository.DeleteUserById(userId);
        }

        public List<User> GetAllUsers()
        {
            return _usersRepository.GetAllUsers().Select(ue => ue.ToDomain()).ToList();
        }

        public User GetUserById(int userId)
        {
            return _usersRepository.GetUserById(userId).ToDomain();
        }

        public User GetUserByName(string username)
        {
            return _usersRepository.GetUserByName(username).ToDomain();
        }

        public void UpdateUser(User user)
        {
            _usersRepository.UpdateUser(user.ToEntity());
        }

        public User DoesUserExist(User user)
        {
            User userDomain = (_usersRepository.GetUserByName(user.Name) ?? _usersRepository.GetUserByEmail(user.Email)).ToDomain();

            if(userDomain is null)
            {
                throw new ArgumentException("There's no user with such name or email");
            }
            if(user.Password == userDomain.Password)
            {
                return userDomain;
            }

            return null;
        }
    }
}
