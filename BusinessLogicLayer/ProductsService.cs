﻿using BusinessLogicLayer.Abstract;
using DataAccessLayer.Repositories.Abstract;
using Domain;
using Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer
{
    public class ProductsService : IProductsService
    {
        private readonly IProductsRepository _productsRepository;

        public ProductsService(IProductsRepository productsRepository)
        {
            _productsRepository = productsRepository;
        }
        public void AddProduct(Product product)
        {
            _productsRepository.AddProduct(product.ToEntity());
        }

        public Product GetProductById(int productId)
        {
            return _productsRepository.GetProductById(productId).ToDomain();
        }

        public Product GetProductByName(string name)
        {
            return _productsRepository.GetProductByName(name).ToDomain();
        }

        public List<Product> GetProducts()
        {
            return _productsRepository.GetAllProducts().Select(pe => pe.ToDomain()).ToList();
        }

        public void UpdateProduct(Product product)
        {
            _productsRepository.UpdateProduct(product.ToEntity());
        }
    }
}
