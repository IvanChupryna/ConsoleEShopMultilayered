﻿using BusinessLogicLayer.Abstract;
using DataAccessLayer.Repositories.Abstract;
using Domain;
using Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer
{
    public class OrdersService : IOrdersService
    {
        private readonly IOrdersRepository _ordersRepository;
        public OrdersService(IOrdersRepository ordersRepository)
        {
            _ordersRepository = ordersRepository;
        }
        public void AddOrder(Order order)
        {
            _ordersRepository.AddOrder(order.ToEntity());
        }

        public void DeleteOrderById(int orderId)
        {
            _ordersRepository.DeleteOrderById(orderId);
        }

        public List<Order> GetAllOrders()
        {
            return _ordersRepository.GetAllOrders().Select(oe => oe.ToDomain()).ToList();
        }

        public Order GetOrderById(int orderId)
        {
            return _ordersRepository.GetOrderById(orderId).ToDomain();
        }

        public List<Order> GetOrdersForUser(int userId)
        {
            return _ordersRepository.GetOrdersForUser(userId).Select(ue => ue.ToDomain()).ToList();
        }

        public void UpdateOrder(Order order)
        {
            _ordersRepository.UpdateOrder(order.ToEntity());
        }
    }
}
