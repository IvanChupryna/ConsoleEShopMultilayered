﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Abstract
{
    public interface IOrdersService
    {
        void AddOrder(Order order);
        void UpdateOrder(Order order);
        List<Order> GetAllOrders();
        List<Order> GetOrdersForUser(int userId);
        Order GetOrderById(int orderId);
        void DeleteOrderById(int orderId);
    }
}
