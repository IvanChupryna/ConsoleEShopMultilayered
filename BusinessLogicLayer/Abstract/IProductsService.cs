﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Abstract
{
    public interface IProductsService
    {
        void AddProduct(Product product);
        List<Product> GetProducts();
        Product GetProductByName(string name);
        Product GetProductById(int productId);
        void UpdateProduct(Product product);

    }
}
