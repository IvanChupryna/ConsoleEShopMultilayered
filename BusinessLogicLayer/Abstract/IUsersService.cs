﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Abstract
{
    public interface IUsersService
    {
        void AddUser(User user);
        void UpdateUser(User user);
        List<User> GetAllUsers();
        User GetUserByName(string username);
        User GetUserById(int userId);
        void DeleteUserById(int userId);
        public User DoesUserExist(User user);
    }
}
