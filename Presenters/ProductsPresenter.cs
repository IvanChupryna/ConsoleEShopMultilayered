﻿using BusinessLogicLayer.Abstract;
using Mappers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Args;
using Views.Abstract;

namespace Presenters
{
    public class ProductsPresenter : IProductsPresenter
    {
        private readonly IProductsView _productsView;
        private readonly IProductsService _productsService;

        public event EventHandler WorkedWithProducts;

        public ProductsPresenter(IProductsView productsView, IProductsService productsService)
        {
            _productsService = productsService;
            _productsView = productsView;
        }

        public void AddProduct(object sender, EventArgs e)
        {
            string productName = _productsView.GetNewProductsName();
            decimal productPrice = _productsView.GetProductsPrice();


            ProductModel productToAdd = new ProductModel();
            productToAdd.Id = new Random().Next();
            productToAdd.Name = productName;
            productToAdd.Price = productPrice;
            _productsService.AddProduct(productToAdd.ToDomain());

            WorkedWithProducts?.Invoke(this, new ResultEventArgs($"Product with id {productToAdd.Id} is successfully added to catalog"));
        }

        public void EditProduct(object sender, EventArgs e)
        {
            ProductModel productToEdit = _productsService.GetProductById(_productsView.GetProductId()).ToModel();
            string modifiedName = _productsView.GetNewProductsName();
            decimal modifiedPrice = _productsView.GetProductsPrice();

            productToEdit.Name = modifiedName;
            productToEdit.Price = modifiedPrice;

            _productsService.UpdateProduct(productToEdit.ToDomain());
            WorkedWithProducts?.Invoke(this, new ResultEventArgs($"Product with id {productToEdit.Id} is successfully modified"));
        }

        public void ShowAllProducts(object sender, EventArgs e)
        {
            List<ProductModel> products = _productsService.GetProducts()
                .Select(p => p.ToModel()).ToList();
            _productsView.ShowProducts(products);

            WorkedWithProducts?.Invoke(this, new ResultEventArgs(string.Empty));
        }

        public void ShowProduct(object sender, EventArgs e)
        {
            ProductModel product = _productsService.GetProductByName(_productsView.GetProductsName()).ToModel();
            _productsView.ShowProducts(new List<ProductModel>() { product });

            WorkedWithProducts?.Invoke(this, new ResultEventArgs(string.Empty));
        }
    }
}
