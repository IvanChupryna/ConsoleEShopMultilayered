﻿using BusinessLogicLayer.Abstract;
using Mappers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Args;
using Views.Abstract;

namespace Presenters
{
    public class UsersPresenter : IUsersPresenter
    {
        private readonly IUsersView _usersView;
        private readonly IUsersService _usersService;

        public event EventHandler DoneWithUsers;

        public UsersPresenter(IUsersView usersView, IUsersService usersService)
        {
            _usersView = usersView;
            _usersService = usersService;
        }

        public void ShowUsers(object sender, EventArgs e)
        {
            _usersView.ShowUsers(_usersService.GetAllUsers().Select(u => u.ToModel()).ToList());
            DoneWithUsers?.Invoke(this, new ResultEventArgs(string.Empty));
        }

        public void ChangeUser(object sender, EventArgs e)
        {
            int userId = sender.GetType().Equals(typeof(Presenters.UserRolePresenter)) ?
                (sender as UserRolePresenter).User.Id : _usersView.GetUserId();

            string modifiedName = _usersView.GetNewName();
            string modifiedEmail = _usersView.GetNewEmail();

            UserModel updatedUser = new UserModel();
            updatedUser.Id = userId;
            updatedUser.Name = modifiedName;
            updatedUser.Email = modifiedEmail;

            _usersService.UpdateUser(updatedUser.ToDomain());
            DoneWithUsers?.Invoke(this, new ResultEventArgs("Your personal info is successfully changed"));
        }
    }
}
