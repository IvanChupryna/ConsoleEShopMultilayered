﻿using BusinessLogicLayer.Abstract;
using Mappers;
using Models;
using System;
using System.Collections.Generic;
using System.Text;
using Utils.Args;
using Views;
using Views.Abstract;

namespace Presenters
{
    public class CartPresenter : ICartPresenter
    {
        private readonly ICartView _cartView;
        private readonly IProductsService _productsService;
        private readonly Dictionary<int, Dictionary<int, int>> _items = new Dictionary<int, Dictionary<int, int>>();

        public event EventHandler UsedCart;

        public CartPresenter(ICartView cartView, IProductsService productsService)
        {
            _cartView = cartView;
            _productsService = productsService;
        }

        public void EditOrderItem(object sender, EventArgs e)
        {
            int userId = sender.GetType().Equals(typeof(Presenters.UserRolePresenter)) ?
                (sender as UserRolePresenter).User.Id : (sender as AdminRolePresenter).User.Id;
            int itemId = _cartView.GetItemId();

            if (_items[userId].ContainsKey(itemId))
            {
                int modifiedQuantity = _cartView.GetItemQuantity();
                _items[userId][itemId] = modifiedQuantity;
                UsedCart?.Invoke(this, new ResultEventArgs($"You changed quantity of item with Id {itemId} to {modifiedQuantity}"));
            }
            else
            {
                new ErrorPresenter(new ErrorView()).GetAndShowError("There's no item with such id");
            }
        }

        public void RemoveOrderItem(object sender, EventArgs e)
        {
            int userId = sender.GetType().Equals(typeof(Presenters.UserRolePresenter)) ?
                (sender as UserRolePresenter).User.Id : (sender as AdminRolePresenter).User.Id;
            int itemId = _cartView.GetItemId();
            _items[userId].Remove(itemId);
            UsedCart?.Invoke(this, new ResultEventArgs($"Item with Id {itemId} is successfully removed from your cart"));
        }

        public void AddItemToCart(object sender, EventArgs e)
        {
            ProductModel item = _productsService.GetProductById(_cartView.GetItemId()).ToModel();
            int quantity = _cartView.GetItemQuantity();
            int userId = sender.GetType().Equals(typeof(Presenters.UserRolePresenter)) ?
                (sender as UserRolePresenter).User.Id : (sender as AdminRolePresenter).User.Id;

            if (!_items.ContainsKey(userId)) 
            {
                _items.Add(userId, new Dictionary<int, int>());
            }
            _items[userId].Add(item.Id, quantity);
            UsedCart?.Invoke(this, new ResultEventArgs($"Item with Id {item.Id} is successfully added to your cart"));
        }

        public void ClearCart(object sender, EventArgs e)
        {
            int userId = sender.GetType().Equals(typeof(Presenters.UserRolePresenter)) ?
                (sender as UserRolePresenter).User.Id : (sender as AdminRolePresenter).User.Id;

            _items[userId].Clear();
            UsedCart?.Invoke(this, new ResultEventArgs("Your cart is successfully cleared"));
        }

        public void ShowItemsInCart(object sender, EventArgs e)
        {
            int userId = sender.GetType().Equals(typeof(Presenters.UserRolePresenter)) ?
                (sender as UserRolePresenter).User.Id : (sender as AdminRolePresenter).User.Id;
            Dictionary<string, int> itemsToShow = new Dictionary<string, int>();

            if(!_items.ContainsKey(userId))
            {
                new ErrorPresenter(new ErrorView()).GetAndShowError("You don't have any items in your cart yet");
                return;
            }

            foreach (var item in _items[userId])
            {
                itemsToShow.Add(_productsService.GetProductById(item.Key).Name, item.Value);
            }

            _cartView.ShowItems(itemsToShow);
            UsedCart?.Invoke(this, new ResultEventArgs(string.Empty));
        }

        public Dictionary<int, int> GetItems(object sender, EventArgs e)
        {
            int userId = sender.GetType().Equals(typeof(Presenters.UserRolePresenter)) ?
                (sender as UserRolePresenter).User.Id : (sender as AdminRolePresenter).User.Id;

            return _items[userId];
        }
    }
}
