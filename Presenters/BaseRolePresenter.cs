﻿using BusinessLogicLayer;
using DataAccessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Utils.Args;
using Views;
using Views.Abstract;

namespace Presenters
{
    public abstract class BaseRolePresenter : IRolePresenter
    {
        protected readonly IBaseRoleView _baseRoleView;
        
        protected BaseRolePresenter(IBaseRoleView baseRoleView)
        {
            _baseRoleView = baseRoleView;
            SubscribeToEvents();
        }

        public void ShowRoleView()
        {
            _baseRoleView.DisplayOptions();
        }

        public void ShowMessage(object sender, EventArgs e)
        {
            _baseRoleView.DisplayMessage((e as ResultEventArgs).Result);
        }

        public void GetUserInput()
        {
            _baseRoleView.GetUserChoice();
        }
        public abstract void ExecuteOption(object sender, EventArgs e);
        private void SubscribeToEvents()
        {
            _baseRoleView.RoleMadeChoice += ExecuteOption;
        }
    }
}
