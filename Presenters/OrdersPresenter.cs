﻿using BusinessLogicLayer.Abstract;
using Mappers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.Args;
using Views.Abstract;

namespace Presenters
{
    public class OrdersPresenter : IOrdersPresenter
    {
        private readonly IOrdersView _ordersView;
        private readonly IOrdersService _ordersService;
        private readonly ICartPresenter _cartPresenter;

        public event EventHandler WorkedWithOrders;

        public OrdersPresenter(IOrdersView ordersView, IOrdersService ordersService, ICartPresenter cartPresenter)
        {
            _ordersService = ordersService;
            _ordersView = ordersView;
            _cartPresenter = cartPresenter;

        }

        public void ShowAllOrders(object sender, EventArgs e)
        {
            _ordersView.ShowOrders(_ordersService.GetAllOrders()
                .Select(o => o.ToModel())
                .ToList());
            WorkedWithOrders?.Invoke(this, new ResultEventArgs(string.Empty));
        }

        public void ShowUserOrders(object sender, EventArgs e)
        {
            int userId = sender.GetType().Equals(typeof(UserRolePresenter)) ? (sender as UserRolePresenter).User.Id : (sender as AdminRolePresenter).User.Id;
            _ordersView.ShowOrders(_ordersService.GetOrdersForUser(userId).Select(o => o.ToModel()).ToList());
            WorkedWithOrders?.Invoke(this, new ResultEventArgs(string.Empty));
        }

        public void PlaceOrder(object sender, EventArgs e)
        {
            int userId = sender.GetType().Equals(typeof(UserRolePresenter)) ? (sender as UserRolePresenter).User.Id : (sender as AdminRolePresenter).User.Id;
            int orderId = new Random().Next();

            OrderModel orderToPlace = new OrderModel();
            orderToPlace.Id = orderId;
            orderToPlace.UserId = userId;
            orderToPlace.Items = _cartPresenter.GetItems(sender, e);
            orderToPlace.CreatedDate = DateTime.Now;
            orderToPlace.Status = "new";

            _ordersService.AddOrder(orderToPlace.ToDomain());
            _cartPresenter.ClearCart(this, new EventArgs());
            WorkedWithOrders?.Invoke(this, new ResultEventArgs($"Order with Id {orderId} is successfully placed"));
        }

        public void CancelOrder(object sender, EventArgs e)
        {
            int orderId = _ordersView.GetOrderToCancel();
            _ordersService.DeleteOrderById(orderId);
            WorkedWithOrders?.Invoke(this, new ResultEventArgs($"Order with Id {orderId} is successfully canceled"));
        }

        public void SetDifferentStatusForOrder(object sender, EventArgs e)
        {
            int orderId = _ordersView.GetOrderToChangeStatus();
            OrderModel updatedOrder = _ordersService.GetOrderById(orderId).ToModel();

            if (sender.GetType().Equals(typeof(UserRolePresenter)))
            {
                updatedOrder.Status = "Recieved";
                _ordersService.UpdateOrder(updatedOrder.ToDomain());
            }
            else
            {
                string status = _ordersView.GetNewStatus();
                updatedOrder.Status = status;
                _ordersService.UpdateOrder(updatedOrder.ToDomain());
            }

            WorkedWithOrders?.Invoke(this, new ResultEventArgs($"Status of order with Id {orderId} chaneg to {updatedOrder.Status}"));
        }
    }
}
