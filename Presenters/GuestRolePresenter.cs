﻿using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Views;

namespace Presenters
{
    public class GuestRolePresenter : BaseRolePresenter
    {
        public event EventHandler ShowingProducts;
        public event EventHandler ShowingProduct;
        public event EventHandler Registrating;
        public event EventHandler Authorizing;
        public event EventHandler Exiting;
        public GuestRolePresenter(GuestRoleView guestRoleView) : base(guestRoleView) { }
        public override void ExecuteOption(object sender, EventArgs e)
        {
            Console.Clear();
            switch((e as UserChoiceEventArgs).UserChoice)
            {
                case "1": ShowingProducts?.Invoke(this, new EventArgs());break;
                case "2": ShowingProduct?.Invoke(this, new EventArgs()); break;
                case "3": Registrating?.Invoke(this, new EventArgs()); break;
                case "4": Authorizing?.Invoke(this, new EventArgs()); break;
                case "5":
                    {
                        Exiting?.Invoke(this, new EventArgs());
                        return;
                    }
            }
            Console.ReadKey();
        }
    }
}
