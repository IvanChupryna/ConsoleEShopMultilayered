﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Views;

namespace Presenters
{
    public class AdminRolePresenter : BaseRolePresenter
    {
        public event EventHandler ShowingProducts;
        public event EventHandler ShowingProduct;
        public event EventHandler AddingProductToCatalog;
        public event EventHandler ModifyingProduct;
        public event EventHandler AddingItemToCart;
        public event EventHandler RemovingItemToCart;
        public event EventHandler EditingItemToCart;
        public event EventHandler ShowingItemsInCart;
        public event EventHandler ClearingCart;
        public event EventHandler PlacingOrder;
        public event EventHandler CancelingOrder;
        public event EventHandler GettingOrderHistory;
        public event EventHandler GettingAllOrdersInfo;
        public event EventHandler ChangingOrderStatus;
        public event EventHandler GettingAllUsersInfo;
        public event EventHandler ChangingUserInfo;
        public event EventHandler LogOut;
        public event EventHandler Exit;
        public UserModel User { get; }
        public AdminRolePresenter(AdminRoleView adminRoleView, UserModel user) : base(adminRoleView)
        {
            User = user;
        }
        public override void ExecuteOption(object sender, EventArgs e)
        {
            Console.Clear();
            switch ((e as UserChoiceEventArgs).UserChoice)
            {
                case "1": ShowingProducts?.Invoke(this, new EventArgs()); break;
                case "2": ShowingProduct?.Invoke(this, new EventArgs()); break;
                case "3": AddingProductToCatalog?.Invoke(this, new EventArgs()); break;
                case "4": AddingItemToCart?.Invoke(this, new EventArgs()); break;
                case "5": RemovingItemToCart?.Invoke(this, new EventArgs()); break;
                case "6": EditingItemToCart?.Invoke(this, new EventArgs()); break;
                case "7": ShowingItemsInCart?.Invoke(this, new EventArgs()); break;
                case "8": ClearingCart?.Invoke(this, new EventArgs()); break;
                case "9": PlacingOrder?.Invoke(this, new EventArgs()); break;
                case "10": GettingOrderHistory?.Invoke(this, new EventArgs()); break;
                case "11": GettingAllOrdersInfo?.Invoke(this, new EventArgs()); break;
                case "12": CancelingOrder?.Invoke(this, new EventArgs()); break;
                case "13": GettingAllUsersInfo?.Invoke(this, new EventArgs()); break;
                case "14": ChangingUserInfo?.Invoke(this, new EventArgs()); break;
                case "15": ModifyingProduct?.Invoke(this, new EventArgs()); break;
                case "16": ChangingOrderStatus?.Invoke(this, new EventArgs()); break;
                case "17": LogOut?.Invoke(this, new EventArgs()); break;
                case "18": Exit?.Invoke(this, new EventArgs()); break;
            }
            Console.ReadKey();
        }
    }
}
