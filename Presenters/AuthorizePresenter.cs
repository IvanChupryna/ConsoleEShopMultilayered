﻿using BusinessLogicLayer.Abstract;
using Mappers;
using Models;
using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Utils.Args;
using Views;
using Views.Abstract;

namespace Presenters
{
    public class AuthorizePresenter : IAuthorizePresenter
    {
        private readonly IAuthorizeView _authorizeView;
        private readonly IUsersService _usersService;


        public event EventHandler AuthorizedForOutput;
        public event EventHandler AuthorizedToChangeRole;

        public AuthorizePresenter(IAuthorizeView authorizeView, IUsersService usersService)
        {
            _authorizeView = authorizeView;
            _usersService = usersService;
        }

        public void GetData(object sender, EventArgs e)
        {
            _authorizeView.GetUserNameOrEmail();
            _authorizeView.GetPassword();

            TryLogin();
        }

        private void TryLogin()
        {
            UserModel userToCheck = _authorizeView.SendUserModel();

            try
            {
                UserModel currentUser = _usersService.DoesUserExist(userToCheck.ToDomain()).ToModel();
                if (currentUser != null)
                {
                    AuthorizedForOutput?.Invoke(this, new ResultEventArgs("Authorization is successful"));
                    AuthorizedToChangeRole?.Invoke(this, new AuthorizeEventArgs(currentUser));
                }
                else 
                {
                    new ErrorPresenter(new ErrorView()).GetAndShowError("Password is incorrect");
                }
            }
            catch (ArgumentException argumentException) 
            {
                new ErrorPresenter(new ErrorView()).GetAndShowError(argumentException.Message);
            }
        }
    }
}
