﻿using BusinessLogicLayer.Abstract;
using Mappers;
using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Utils.Args;
using Views;

namespace Presenters
{
    public class RegisterPresenter : IRegisterPresenter
    {
        private readonly IRegisterView _registerView;
        private readonly IUsersService _usersService;

        public event EventHandler Registered;

        public RegisterPresenter(IRegisterView registerView, IUsersService usersService)
        {
            _registerView = registerView;
            _usersService = usersService;
        }

        public void GetData(object sender, EventArgs e)
        {
            _registerView.GetUserName();
            _registerView.GetEmail();
            _registerView.GetPassword();

            RegisterNewUser();
        }

        private void RegisterNewUser()
        {
            _usersService.AddUser(_registerView.SendUserModel().ToDomain());
            Registered?.Invoke(this, new ResultEventArgs("Registration is successful"));
        }
    }
}
