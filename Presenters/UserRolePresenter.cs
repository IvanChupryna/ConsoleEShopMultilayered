﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Utils.Args;
using Views;

namespace Presenters
{
    public class UserRolePresenter : BaseRolePresenter
    {
        public event EventHandler ShowingProducts;
        public event EventHandler ShowingProduct;
        public event EventHandler AddingItemToCart;
        public event EventHandler RemovingItemToCart;
        public event EventHandler EditingItemToCart;
        public event EventHandler ShowingItemsInCart;
        public event EventHandler ClearingCart;
        public event EventHandler PlacingOrder;
        public event EventHandler CancelingOrder;
        public event EventHandler GettingOrderHistory;
        public event EventHandler ChangingOrderStatus;
        public event EventHandler ChangingUserInfo;
        public event EventHandler LogOut;
        public event EventHandler Exit;
        public UserModel User { get; }

        public UserRolePresenter(UserRoleView userRoleView, UserModel user) : base(userRoleView) 
        {
            User = user;
        }
        public override void ExecuteOption(object sender, EventArgs e)
        {
            Console.Clear();
            switch((e as UserChoiceEventArgs).UserChoice)
            {
                case "1": ShowingProducts?.Invoke(this, new EventArgs()); break;
                case "2": ShowingProduct?.Invoke(this, new EventArgs()); break;
                case "3": AddingItemToCart?.Invoke(this, new EventArgs()); break;
                case "4": RemovingItemToCart?.Invoke(this, new EventArgs()); break;
                case "5": EditingItemToCart?.Invoke(this, new EventArgs()); break;
                case "6": ShowingItemsInCart?.Invoke(this, new EventArgs()); break;
                case "7": ClearingCart?.Invoke(this, new EventArgs()); break;
                case "8": PlacingOrder?.Invoke(this, new EventArgs()); break;
                case "9": GettingOrderHistory?.Invoke(this, new EventArgs()); break;
                case "10": CancelingOrder?.Invoke(this, new EventArgs()); break;
                case "11": ChangingOrderStatus?.Invoke(this, new EventArgs()); break;
                case "12": ChangingUserInfo?.Invoke(this, new EventArgs()); break;
                case "13": LogOut?.Invoke(this, new EventArgs()); break;
                case "14": Exit?.Invoke(this, new EventArgs()); break;
            }
            Console.ReadKey();
        }
    }
}
