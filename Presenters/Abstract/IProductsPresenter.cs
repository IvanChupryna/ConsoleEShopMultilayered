﻿using System;

namespace Presenters
{
    public interface IProductsPresenter
    {
        event EventHandler WorkedWithProducts;

        void AddProduct(object sender, EventArgs e);
        void EditProduct(object sender, EventArgs e);
        void ShowAllProducts(object sender, EventArgs e);
        void ShowProduct(object sender, EventArgs e);
    }
}