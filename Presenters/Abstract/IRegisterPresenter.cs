﻿using System;

namespace Presenters
{
    public interface IRegisterPresenter
    {
        void GetData(object sender, EventArgs e);
    }
}