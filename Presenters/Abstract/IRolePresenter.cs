﻿using System;
using Utils;

namespace Presenters
{
    public interface IRolePresenter
    { 
        void ShowRoleView();
        void GetUserInput();
        void ShowMessage(object sender, EventArgs e);
        void ExecuteOption(object sender, EventArgs e);
    }
}