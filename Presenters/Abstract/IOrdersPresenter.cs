﻿using System;

namespace Presenters
{
    public interface IOrdersPresenter
    {
        event EventHandler WorkedWithOrders;

        void CancelOrder(object sender, EventArgs e);
        void PlaceOrder(object sender, EventArgs e);
        void SetDifferentStatusForOrder(object sender, EventArgs e);
        void ShowAllOrders(object sender, EventArgs e);
        void ShowUserOrders(object sender, EventArgs e);
    }
}