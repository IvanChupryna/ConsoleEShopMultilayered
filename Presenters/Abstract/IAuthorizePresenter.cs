﻿using System;

namespace Presenters
{
    public interface IAuthorizePresenter
    {
        event EventHandler AuthorizedForOutput;
        public event EventHandler AuthorizedToChangeRole;

        void GetData(object sender, EventArgs e);
    }
}