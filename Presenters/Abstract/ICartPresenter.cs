﻿using System;
using System.Collections.Generic;

namespace Presenters
{
    public interface ICartPresenter
    {
        event EventHandler UsedCart;

        void AddItemToCart(object sender, EventArgs e);
        void ClearCart(object sender, EventArgs e);
        void EditOrderItem(object sender, EventArgs e);
        Dictionary<int, int> GetItems(object sender, EventArgs e);
        void RemoveOrderItem(object sender, EventArgs e);
        void ShowItemsInCart(object sender, EventArgs e);
    }
}