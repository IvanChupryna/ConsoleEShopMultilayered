﻿using System;

namespace Presenters
{
    public interface IUsersPresenter
    {
        event EventHandler DoneWithUsers;

        void ChangeUser(object sender, EventArgs e);
        void ShowUsers(object sender, EventArgs e);
    }
}