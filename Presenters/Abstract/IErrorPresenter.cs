﻿using Utils;

namespace Presenters
{
    public interface IErrorPresenter
    {

        void GetAndShowError(string errorMessage);
    }
}