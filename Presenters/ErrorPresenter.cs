﻿using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Views.Abstract;

namespace Presenters
{
    public class ErrorPresenter : IErrorPresenter
    {
        private readonly IErrorView _errorView;

        public ErrorPresenter(IErrorView errorView)
        {
            _errorView = errorView;
        }

        public void GetAndShowError(string errorMessage)
        {
            _errorView.GetAndShowError(errorMessage);
        }
    }
}
