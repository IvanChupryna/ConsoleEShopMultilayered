﻿using BusinessLogicLayer.Abstract;
using Mappers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controllers
{
    public class ProductsController
    {
        private readonly IProductsService _productsService;

        public ProductsController(IProductsService productsService)
        {
            _productsService = productsService;
        }

        public void AddProduct(ProductModel productModel)
        {
            _productsService.AddProduct(productModel.ToDomain());
        }

        public List<ProductModel> GetProducts()
        {
            return _productsService.GetProducts().Select(pm => pm.ToModel()).ToList();
        }

        public ProductModel GetProductById(int productId)
        {
            return _productsService.GetProductById(productId).ToModel();
        }

        public ProductModel GetProductByName(string name)
        {
            return _productsService.GetProductByName(name).ToModel();
        }
        public void UpdateProduct(ProductModel productModel)
        {
            _productsService.UpdateProduct(productModel.ToDomain());
        }
    }
}
