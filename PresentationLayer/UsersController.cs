﻿using BusinessLogicLayer.Abstract;
using Mappers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controllers
{
    public class UsersController
    {
        private readonly IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        public void AddUser(UserModel userModel)
        {
            _usersService.AddUser(userModel.ToDomain());
        }

        public void DeleteUserById(int userId)
        {
            _usersService.DeleteUserById(userId);
        }

        public void UpdateUser(UserModel userModel)
        {
            _usersService.UpdateUser(userModel.ToDomain());
        }

        public List<UserModel> GetAllUsers()
        {
            return _usersService.GetAllUsers().Select(um => um.ToModel()).ToList();
        }

        public UserModel GetUserByName(string username)
        {
            return _usersService.GetUserByName(username).ToModel();
        }

        public UserModel GetUserById(int userId)
        {
            return _usersService.GetUserById(userId).ToModel();
        }
    }
}
