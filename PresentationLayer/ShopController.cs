﻿using BusinessLogicLayer.Abstract;
using Mappers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Views.Abstract;

namespace Controllers
{
    class ShopController
    {
        private readonly ProductsController _productsController;
        private readonly UsersController _usersController;
        private readonly OrdersController _ordersController;
        private readonly IView _view;

        public ShopController(ProductsController productsController, UsersController usersController, OrdersController ordersController, IView view)
        {
            _productsController = productsController;
            _usersController = usersController;
            _ordersController = ordersController;
            _view = view;
        }

        
    }
}
