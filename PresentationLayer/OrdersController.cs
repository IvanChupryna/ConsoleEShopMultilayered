﻿using BusinessLogicLayer.Abstract;
using Mappers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controllers
{
    public class OrdersController
    {
        private readonly IOrdersService _ordersService;

        public OrdersController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        public void AddOrder(OrderModel orderModel)
        {
            _ordersService.AddOrder(orderModel.ToDomain());
        }

        public void UpdateOrder(OrderModel orderModel)
        {
            _ordersService.UpdateOrder(orderModel.ToDomain());
        }

        public List<OrderModel> GetAllOrders()
        {
            return _ordersService.GetAllOrders().Select(om => om.ToModel()).ToList();
        }

        public List<OrderModel> GetOrdersForUser(int userId)
        {
            return _ordersService.GetOrdersForUser(userId).Select(om => om.ToModel()).ToList();
        }

        public OrderModel GetOrderById(int orderId)
        {
            return _ordersService.GetOrderById(orderId).ToModel();
        }

        public void DeleteOrder(int orderId)
        {
            _ordersService.DeleteOrderById(orderId);
        }
    }
}
