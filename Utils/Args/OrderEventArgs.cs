﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils.Args
{
    public class OrderEventArgs : EventArgs
    {
        public int UserId { get; }
        public OrderEventArgs(int userId)
        {
            UserId = userId;
        }
    }
}
