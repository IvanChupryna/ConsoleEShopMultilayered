﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    public class UserChoiceEventArgs : EventArgs
    {
        public string UserChoice { get; }
        public UserChoiceEventArgs(string userChoice)
        {
            UserChoice = userChoice;
        }
    }
}
