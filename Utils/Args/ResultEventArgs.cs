﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils.Args
{
    public class ResultEventArgs : EventArgs
    {
        public string Result { get; }
        public ResultEventArgs(string result)
        {
            Result = result;
        }
    }
}
