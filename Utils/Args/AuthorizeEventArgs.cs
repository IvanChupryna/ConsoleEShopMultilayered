﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    public class AuthorizeEventArgs : EventArgs
    {
        public UserModel User { get; }

        public AuthorizeEventArgs(UserModel user)
        {
            User = user;
        }
    }
}
