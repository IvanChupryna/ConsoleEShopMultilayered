﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    public class ErrorEventArgs : EventArgs
    {
        public string ErrorMessage { get; }
        public ErrorEventArgs(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }
}
