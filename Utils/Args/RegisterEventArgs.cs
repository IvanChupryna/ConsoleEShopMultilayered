﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    public class RegisterEventArgs : EventArgs
    {
        public UserModel User { get; }

        public RegisterEventArgs(UserModel user)
        {
            User = user;
        }
    }
}
