﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public Dictionary<int, int> Items { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }
    }
}
