﻿using BusinessLogicLayer;
using DataAccessLayer.Repositories;
using Models;
using Presenters;
using System;
using Utils;
using Views;

namespace ConsoleEShopMultilayered
{
    class Program
    {
        static bool flag = true;
        static BaseRolePresenter rolePresenter;
        static RegisterPresenter registerPresenter;
        static AuthorizePresenter authorizePresenter;
        static ProductsPresenter productsPresenter;
        static CartPresenter cartPresenter;
        static OrdersPresenter ordersPresenter;
        static UsersPresenter usersPresenter;
        static void Main(string[] args)
        {
            UsersRepository usersRepository = new UsersRepository();
            ProductsRepository productsRepository = new ProductsRepository();
            OrdersRepository ordersRepository = new OrdersRepository();

            UsersService usersService = new UsersService(usersRepository);
            ProductsService productsService = new ProductsService(productsRepository);
            OrdersService ordersService = new OrdersService(ordersRepository);

            registerPresenter = new RegisterPresenter(new RegisterView(), usersService);
            authorizePresenter = new AuthorizePresenter(new AuthorizeView(), usersService);
            productsPresenter = new ProductsPresenter(new ProductsView(), productsService);
            cartPresenter = new CartPresenter(new CartView(), productsService);
            ordersPresenter = new OrdersPresenter(new OrdersView(), ordersService, cartPresenter);
            usersPresenter = new UsersPresenter(new UsersView(), usersService);

            rolePresenter = new GuestRolePresenter(new GuestRoleView());

            (rolePresenter as GuestRolePresenter).Registrating += registerPresenter.GetData;
            (rolePresenter as GuestRolePresenter).Authorizing += authorizePresenter.GetData;
            (rolePresenter as GuestRolePresenter).Exiting += Exit;
            (rolePresenter as GuestRolePresenter).ShowingProducts += productsPresenter.ShowAllProducts;
            (rolePresenter as GuestRolePresenter).ShowingProduct += productsPresenter.ShowProduct;

            registerPresenter.Registered += rolePresenter.ShowMessage;
            authorizePresenter.AuthorizedForOutput += rolePresenter.ShowMessage;
            authorizePresenter.AuthorizedToChangeRole += Authorize;
            productsPresenter.WorkedWithProducts += rolePresenter.ShowMessage;
            cartPresenter.UsedCart += rolePresenter.ShowMessage;
            ordersPresenter.WorkedWithOrders += rolePresenter.ShowMessage;
            usersPresenter.DoneWithUsers += rolePresenter.ShowMessage;

            while (flag)
            {
                rolePresenter.ShowRoleView();
                rolePresenter.GetUserInput();
            }
        }

        public static void Authorize(object sender, EventArgs e)
        {
            UserModel currentUser = (e as AuthorizeEventArgs).User;
            if(currentUser.IsAdmin)
            {
                rolePresenter = new AdminRolePresenter(new AdminRoleView(), currentUser);
                (rolePresenter as AdminRolePresenter).ShowingProducts += productsPresenter.ShowAllProducts;
                (rolePresenter as AdminRolePresenter).ShowingProduct += productsPresenter.ShowProduct;
                (rolePresenter as AdminRolePresenter).AddingProductToCatalog += productsPresenter.AddProduct;
                (rolePresenter as AdminRolePresenter).AddingItemToCart += cartPresenter.AddItemToCart;
                (rolePresenter as AdminRolePresenter).RemovingItemToCart += cartPresenter.RemoveOrderItem;
                (rolePresenter as AdminRolePresenter).EditingItemToCart += cartPresenter.EditOrderItem;
                (rolePresenter as AdminRolePresenter).ClearingCart += cartPresenter.ClearCart;
                (rolePresenter as AdminRolePresenter).ShowingItemsInCart += cartPresenter.ShowItemsInCart;
                (rolePresenter as AdminRolePresenter).PlacingOrder += ordersPresenter.PlaceOrder;
                (rolePresenter as AdminRolePresenter).CancelingOrder += ordersPresenter.CancelOrder;
                (rolePresenter as AdminRolePresenter).ChangingOrderStatus += ordersPresenter.SetDifferentStatusForOrder;
                (rolePresenter as AdminRolePresenter).GettingOrderHistory += ordersPresenter.ShowUserOrders;
                (rolePresenter as AdminRolePresenter).GettingAllOrdersInfo += ordersPresenter.ShowAllOrders;
                (rolePresenter as AdminRolePresenter).ChangingUserInfo += usersPresenter.ChangeUser;
                (rolePresenter as AdminRolePresenter).GettingAllUsersInfo += usersPresenter.ShowUsers;
                (rolePresenter as AdminRolePresenter).ModifyingProduct += productsPresenter.EditProduct;
                (rolePresenter as AdminRolePresenter).LogOut += LogOut;
                (rolePresenter as AdminRolePresenter).Exit += Exit;
            }
            else
            {
                rolePresenter = new UserRolePresenter(new UserRoleView(), currentUser);
                (rolePresenter as UserRolePresenter).ShowingProducts += productsPresenter.ShowAllProducts;
                (rolePresenter as UserRolePresenter).ShowingProduct += productsPresenter.ShowProduct;
                (rolePresenter as UserRolePresenter).AddingItemToCart += cartPresenter.AddItemToCart;
                (rolePresenter as UserRolePresenter).RemovingItemToCart += cartPresenter.RemoveOrderItem;
                (rolePresenter as UserRolePresenter).EditingItemToCart += cartPresenter.EditOrderItem;
                (rolePresenter as UserRolePresenter).ClearingCart += cartPresenter.ClearCart;
                (rolePresenter as UserRolePresenter).ShowingItemsInCart += cartPresenter.ShowItemsInCart;
                (rolePresenter as UserRolePresenter).PlacingOrder += ordersPresenter.PlaceOrder;
                (rolePresenter as UserRolePresenter).CancelingOrder += ordersPresenter.CancelOrder;
                (rolePresenter as UserRolePresenter).ChangingOrderStatus += ordersPresenter.SetDifferentStatusForOrder;
                (rolePresenter as UserRolePresenter).GettingOrderHistory += ordersPresenter.ShowUserOrders;
                (rolePresenter as UserRolePresenter).ChangingUserInfo += usersPresenter.ChangeUser;
                (rolePresenter as UserRolePresenter).LogOut += LogOut;
                (rolePresenter as UserRolePresenter).Exit += Exit;
            }
        }

        public static void LogOut(object sender, EventArgs e)
        {
            rolePresenter = new GuestRolePresenter(new GuestRoleView());

            (rolePresenter as GuestRolePresenter).Registrating += registerPresenter.GetData;
            (rolePresenter as GuestRolePresenter).Authorizing += authorizePresenter.GetData;
            (rolePresenter as GuestRolePresenter).Exiting += Exit;
            (rolePresenter as GuestRolePresenter).ShowingProducts += productsPresenter.ShowAllProducts;
            (rolePresenter as GuestRolePresenter).ShowingProduct += productsPresenter.ShowProduct;
        }

        public static void Exit(object sender, EventArgs e)
        {
            flag = false;
        }
    }
}
