﻿using System;
using System.Collections.Generic;
using System.Text;
using Views.Abstract;

namespace Views
{
    public class ErrorView : IErrorView
    {
        public void GetAndShowError(string errorMessage)
        {
            Console.Clear();

            Console.WriteLine(errorMessage);
            Console.WriteLine("Try something else...");
        }
    }
}
