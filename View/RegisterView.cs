﻿using Models;
using System;

namespace Views
{
    public class RegisterView : IRegisterView
    {
        private string _userName;
        private string _userEmail;
        private string _userPassword;

        public RegisterView() { }

        public void GetEmail()
        {
            Console.Write("Enter your email: ");
            _userEmail = Console.ReadLine().TrimEnd();
        }

        public void GetPassword()
        {
            Console.Write("Enter your password: ");
            _userPassword = Console.ReadLine();
        }

        public void GetUserName()
        {
            Console.Write("Enter your username: ");
            _userName = Console.ReadLine().TrimEnd();
        }

        public UserModel SendUserModel()
        {
            UserModel user = new UserModel();
            user.Id = new Random().Next();
            user.Name = _userName;
            user.Email = _userEmail;
            user.Password = _userPassword;

            return user;
        }
    }
}
