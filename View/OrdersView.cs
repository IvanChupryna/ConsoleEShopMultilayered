﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using Views.Abstract;

namespace Views
{
    public class OrdersView : IOrdersView
    {
        public void ShowOrders(List<OrderModel> ordersToShow)
        {
            Console.WriteLine("Orders:");
            foreach(var order in ordersToShow)
            {
                Console.WriteLine($"\tOrder Id: {order.Id}, User Id: {order.UserId}, Created Date: {order.CreatedDate}, Status: {order.Status}");
            }
        }

        public int GetOrderToCancel()
        {
            Console.Write("Enter Id of the order you want to cancel: ");
            return Convert.ToInt32(Console.ReadLine().TrimEnd());
        }

        public int GetOrderToChangeStatus()
        {
            Console.Write("Enter Id of the order status of which you want to change: ");
            return Convert.ToInt32(Console.ReadLine().TrimEnd());
        }

        public string GetNewStatus()
        {
            Console.Write("Enter Status: ");
            return Console.ReadLine().TrimEnd();
        }
    }
}
