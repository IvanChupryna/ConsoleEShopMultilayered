﻿using System;
using System.Collections.Generic;
using System.Text;
using Views.Abstract;

namespace Views
{
    public class CartView : ICartView
    {
        public void ShowItems(Dictionary<string, int> items)
        {
            Console.WriteLine("Products in your cart: ");
            foreach(var item in items)
            {
                Console.WriteLine($"\tName: {item.Key}, Quantity: {item.Value}");
            }
        }
        public int GetItemId() 
        {
            Console.Write("Enter Id of the product: ");
            return Convert.ToInt32(Console.ReadLine());
        }
        public int GetItemQuantity() 
        {
            Console.Write("Enter Quantity of the product: ");
            return Convert.ToInt32(Console.ReadLine());
        }
    }
}
