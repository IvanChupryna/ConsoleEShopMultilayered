﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using Views.Abstract;

namespace Views
{
    public class UsersView : IUsersView
    {
        public void ShowUsers(List<UserModel> usersToShow)
        {
            Console.WriteLine("Users:");
            foreach(var user in usersToShow)
            {
                Console.WriteLine($"\tUser Id: {user.Id}, User Email: {user.Email}, User Password: {user.Password}, Is Admin: {user.IsAdmin}");
            }
        }

        public string GetNewName()
        {
            Console.Write("Enter new name: ");
            return Console.ReadLine().TrimEnd();
        }

        public string GetNewEmail()
        {
            Console.Write("Enter new email: ");
            return Console.ReadLine().TrimEnd();
        }

        public int GetUserId()
        {
            Console.Write("Enter user's Id: ");
            return Convert.ToInt32(Console.ReadLine().TrimEnd());
        }
    }
}
