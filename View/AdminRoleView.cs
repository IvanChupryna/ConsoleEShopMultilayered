﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Views
{
    public class AdminRoleView : BaseRoleView
    {
        public override void DisplayOptions()
        {
            Console.Clear();
            Console.WriteLine("1 - Get Available Products");
            Console.WriteLine("2 - Search Product");
            Console.WriteLine("3 - Add Product To Catalog");
            Console.WriteLine("4 - Add Product To Cart");
            Console.WriteLine("5 - Remove Product From Cart");
            Console.WriteLine("6 - Edit Product Quantity In Cart");
            Console.WriteLine("7 - Show Products In Cart");
            Console.WriteLine("8 - Clear Cart");
            Console.WriteLine("9 - Place Order");
            Console.WriteLine("10 - Get Orders History");
            Console.WriteLine("11 - Get All Orders");
            Console.WriteLine("12 - Cancel Order");
            Console.WriteLine("13 - Show All Users Info");
            Console.WriteLine("14 - Change User Info");
            Console.WriteLine("15 - Change Product Info");
            Console.WriteLine("16 - Change Order Status");
            Console.WriteLine("17 - Log Out");
            Console.WriteLine("18 - Exit");
        }
    }
}
