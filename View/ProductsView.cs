﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using Views.Abstract;

namespace Views
{
    public class ProductsView : IProductsView
    {
        public string GetProductsName()
        {
            Console.Write("Enter name of a product you want to find: ");
            return Console.ReadLine();
        }

        public string GetNewProductsName()
        {
            Console.Write("Enter name of a product you want to add: ");
            return Console.ReadLine();
        }

        public void ShowProducts(List<ProductModel> products)
        {
            Console.WriteLine("Products you were searching for: ");
            foreach(var product in products)
            {
                Console.WriteLine($"\tProduct Id: {product.Id}, Product name: {product.Name}, Price: {product.Price}");
            }
        }

        public decimal GetProductsPrice()
        {
            Console.Write("Enter price of a new product: ");
            return Convert.ToDecimal(Console.ReadLine());
        }

        public int GetProductId()
        {
            Console.Write("Enter id of a product you want to edit: ");
            return Convert.ToInt32(Console.ReadLine());
        }
    }
}
