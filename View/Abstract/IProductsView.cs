﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Views.Abstract
{
    public interface IProductsView
    {
        public void ShowProducts(List<ProductModel> products);
        public string GetProductsName();
        public decimal GetProductsPrice();
        public int GetProductId();
        public string GetNewProductsName();
    }
}
