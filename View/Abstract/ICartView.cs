﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Views.Abstract
{
    public interface ICartView
    {
        void ShowItems(Dictionary<string, int> items);
        int GetItemId();
        int GetItemQuantity();
    }
}
