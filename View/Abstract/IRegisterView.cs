﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Views
{
    public interface IRegisterView
    {
        public void GetUserName();
        public void GetEmail();
        public void GetPassword();
        public UserModel SendUserModel();
    }
}
