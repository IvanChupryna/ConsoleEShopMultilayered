﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Views.Abstract
{
    public interface IUsersView
    {
        public void ShowUsers(List<UserModel> usersToShow);
        public string GetNewName();
        public string GetNewEmail();
        public int GetUserId();
    }
}
