﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Views.Abstract
{
    public interface IErrorView
    {
        public void GetAndShowError(string errorMessage);
    }
}
