﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Views.Abstract
{
    public interface IBaseRoleView
    {
        public event EventHandler RoleMadeChoice;

        void DisplayMessage(string message);
        void DisplayOptions();
        void GetUserChoice();
    }
}
