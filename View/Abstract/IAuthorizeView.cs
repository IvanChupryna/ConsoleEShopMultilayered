﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Views.Abstract
{
    public interface IAuthorizeView
    {
        void GetUserNameOrEmail();
        void GetPassword();
        UserModel SendUserModel();
    }
}
