﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Views.Abstract
{
    public interface IOrdersView
    {
        void ShowOrders(List<OrderModel> ordersToShow);
        public int GetOrderToCancel();
        public int GetOrderToChangeStatus();
        public string GetNewStatus();
    }
}
