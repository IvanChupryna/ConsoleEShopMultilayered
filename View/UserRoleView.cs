﻿using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Views.Abstract;

namespace Views
{
    public class UserRoleView : BaseRoleView
    {
        public override void DisplayOptions()
        {
            Console.Clear();
            Console.WriteLine("1 - Get Available Products");
            Console.WriteLine("2 - Search Product");
            Console.WriteLine("3 - Add Product To Cart");
            Console.WriteLine("4 - Remove Product From Cart");
            Console.WriteLine("5 - Edit Product Quantity In Cart");
            Console.WriteLine("6 - Show Products In Cart");
            Console.WriteLine("7 - Clear Cart");
            Console.WriteLine("8 - Place Order");
            Console.WriteLine("9 - Get Orders History");
            Console.WriteLine("10 - Cancel Order");
            Console.WriteLine("11 - Set Recieved Status To Order");
            Console.WriteLine("12 - Change Personal Info");
            Console.WriteLine("13 - Log Out");
            Console.WriteLine("14 - Exit");
        }
    }
}
