﻿using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Views.Abstract;

namespace Views
{
    public abstract class BaseRoleView : IBaseRoleView
    {
        public event EventHandler RoleMadeChoice;

        public abstract void DisplayOptions();

        public void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }

        public void GetUserChoice()
        {
            Console.Write("What do you want to do - ");
            string userChoice = Console.ReadLine().Trim();
            
            RoleMadeChoice?.Invoke(this, new UserChoiceEventArgs(userChoice));
        }

    }
}
