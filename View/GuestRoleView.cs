﻿using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Views.Abstract;

namespace Views
{
    public class GuestRoleView : BaseRoleView
    {
        public override void DisplayOptions()
        {
            Console.Clear();
            Console.WriteLine("1 - Get Available Products");
            Console.WriteLine("2 - Search Product");
            Console.WriteLine("3 - Register");
            Console.WriteLine("4 - Authorize");
            Console.WriteLine("5 - Exit Shop");
        }
    }
}
