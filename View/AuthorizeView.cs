﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using Utils;
using Views.Abstract;

namespace Views
{
    public class AuthorizeView : IAuthorizeView
    {
        private string _userNameOrEmail;
        private string _userPassword;
        public void GetPassword()
        {
            Console.Write("Enter your password: ");
            _userPassword = Console.ReadLine();
        }

        public void GetUserNameOrEmail()
        {
            Console.Write("Enter your username or email: ");
            _userNameOrEmail = Console.ReadLine().TrimEnd();
        }

        public UserModel SendUserModel()
        {
            UserModel user = new UserModel();

            if (_userNameOrEmail.Contains("@"))
            {
                user.Email = _userNameOrEmail;
            }
            else
            {
                user.Name = _userNameOrEmail;
            }
            user.Password = _userPassword;

            return user;
        }
    }
}
