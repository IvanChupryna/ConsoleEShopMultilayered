﻿using Domain;
using Entities;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mappers
{
    public static class OrderMapper
    {
        public static OrderModel ToModel(this Order order)
        {
            return new OrderModel
            {
                Id = order.Id,
                UserId = order.UserId,
                Items = order.Items,
                CreatedDate = order.CreatedDate,
                Status = order.Status
            };
        }
        public static OrderEntity ToEntity(this Order order)
        {
            return new OrderEntity()
            {
                Id = order.Id,
                UserId = order.UserId,
                Items = order.Items,
                CreatedDate = order.CreatedDate,
                Status = order.Status
            };
        }

        public static Order ToDomain(this OrderEntity orderEntity)
        {
            return new Order
            {
                Id = orderEntity.Id,
                UserId = orderEntity.UserId,
                Items = orderEntity.Items,
                CreatedDate = orderEntity.CreatedDate,
                Status = orderEntity.Status
            };
        }

        public static Order ToDomain(this OrderModel orderModel)
        {
            return new Order
            {
                Id = orderModel.Id,
                UserId = orderModel.UserId,
                Items = orderModel.Items,
                CreatedDate = orderModel.CreatedDate,
                Status = orderModel.Status
            };
        }
    }
}
