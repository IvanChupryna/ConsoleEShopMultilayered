﻿using Domain;
using Entities;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mappers
{
    public static class UserMapper
    {
        public static UserModel ToModel(this User user)
        {
            return new UserModel()
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
                Password = user.Password,
                IsAdmin = user.IsAdmin
            };
        }
        public static UserEntity ToEntity(this User user)
        {
            return new UserEntity()
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
                Password = user.Password,
                IsAdmin = user.IsAdmin
            };
        }

        public static User ToDomain(this UserEntity userEntity)
        {
            return new User
            {
                Id = userEntity.Id,
                Name = userEntity.Name,
                Email = userEntity.Email,
                Password = userEntity.Password,
                IsAdmin = userEntity.IsAdmin
            };
        }

        public static User ToDomain(this UserModel userModel)
        {
            return new User
            {
                Id = userModel.Id,
                Name = userModel.Name,
                Email = userModel.Email,
                Password = userModel.Password,
                IsAdmin = userModel.IsAdmin
            };
        }
    }
}
