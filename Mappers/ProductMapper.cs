﻿using Domain;
using Entities;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mappers
{
    public static class ProductMapper
    {
        public static ProductEntity ToEntity(this Product product)
        {
            return new ProductEntity()
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price
            };
        }

        public static ProductModel ToModel(this Product product)
        {
            return new ProductModel
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price
            };
        }

        public static Product ToDomain(this ProductEntity productEntity)
        {
            return new Product
            {
                Id = productEntity.Id,
                Name = productEntity.Name,
                Price = productEntity.Price
            };
        }
        public static Product ToDomain(this ProductModel productModel)
        {
            return new Product
            {
                Id = productModel.Id,
                Name = productModel.Name,
                Price = productModel.Price
            };
        }
    }
}
